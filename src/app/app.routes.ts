import {mapToCanActivate, Routes} from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AppComponent } from './app.component';
import {AuthGuard} from "./services/auth.guard";

export const AppRoutes: Routes = [
  { path: '', component: AppComponent },         // Route racine pour AppComponent
  { path: 'login', component: LoginComponent },  // Route pour LoginComponent
  { path: 'register', component: RegisterComponent } // Route pour RegisterComponent
];
