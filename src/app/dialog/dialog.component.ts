import {Component, EventEmitter, Inject, Output} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  MAT_DIALOG_DATA,
  MatDialogActions,
  MatDialogClose,
  MatDialogContent,
  MatDialogRef, MatDialogTitle
} from "@angular/material/dialog";
import {MatInputModule} from "@angular/material/input";
import {FormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {FirebaseService} from "../services/firebase-service.service";
import { MatSelectModule } from '@angular/material/select';


interface Task {
  id: string;
  assignedUserId: string;
  description: string;
  status: string;
  title: string;
}

@Component({
  selector: 'app-dialog',
  standalone: true,
  imports: [CommonModule, MatDialogContent, MatInputModule, MatDialogActions, MatDialogClose, FormsModule, MatDialogTitle, MatButtonModule,MatSelectModule],
  templateUrl: './dialog.component.html',
  styleUrl: './dialog.component.css'
})
export class DialogComponent {
  task: Task = {id: '', assignedUserId: '', title: '', description: '', status: ''};
  users : any[] = [];
  ngOnInit(): void {
    this.loadUsers();
  }

  loadUsers() {
    this.firebaseService.getUsers().subscribe(data => {
      console.log(data, "dataaaa");
      this.users = data;
    });
  }


  constructor(public dialogRef: MatDialogRef<DialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private firebaseService: FirebaseService) {
    if (data) {
      this.task = { ...data };
    }
  }

  onCancel() {
    this.dialogRef.close();
  }

  save() {
    console.log(this.task)
    this.firebaseService.saveTask(this.task).then(() => {
      this.dialogRef.close();
    }).catch(error => {
      console.error("Erreur lors de l'enregistrement de la tâche: ", error);
    });
  }
}
