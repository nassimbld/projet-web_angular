import {Component, Input, OnInit, Output} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import {HeaderComponent} from "./header/header.component";
import {StatusComponent} from "./status/status.component";
import {CdkDragDrop, CdkDropList, CdkDropListGroup, moveItemInArray, transferArrayItem} from "@angular/cdk/drag-drop";
import { FirebaseService } from './services/firebase-service.service';
import {LoginComponent} from "./login/login.component";
import {RegisterComponent} from "./register/register.component";
import { MatSelectModule } from '@angular/material/select';

interface Task {
  id: string;
  assignedUserId: string;
  description: string;
  status: string;
  title: string;
}

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet, HeaderComponent, StatusComponent, CdkDropList, CdkDropListGroup, LoginComponent, RegisterComponent, MatSelectModule],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'projet-web-angular';

  tasks: Task[] = [];

  toDoList: Task[] = [];
  inProgressList: Task[] = [];
  inReviewList: Task[] = [];
  doneList: Task[] = [];

  constructor(private firebaseService: FirebaseService) {  }

  ngOnInit(): void {
    this.loadTasks();
  }

  loadTasks() {
    this.firebaseService.getTasks().subscribe(data => {
      this.tasks = data;
      this.refreshTaskLists();
    });
  }

  drop(event: CdkDragDrop<Task[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      const draggedElement = event.item.element.nativeElement;

      const taskId = draggedElement.getAttribute('data-task-id');
      const destinationList = event.container.id;

      let newStatus: string;
      switch(destinationList) {
        case 'toDoList':
          newStatus = 'To Do';
          break;
        case 'inProgressList':
          newStatus = 'In Progress';
          break;
        case 'inReviewList':
          newStatus = 'In Review';
          break;
        case 'doneList':
          newStatus = 'Done';
          break;
        default:
          newStatus = '';
          break;
      }

      if (taskId && newStatus) {
        this.firebaseService.updateStatusTask(taskId, { status: newStatus })
          .then(() => {
            console.log('Tâche mise à jour avec succès', newStatus);

            const task = this.tasks.find(t => t.id === taskId);
            if (task) {
              task.status = newStatus;
            }

            this.refreshTaskLists();
          })
          .catch(error => {
            console.error('Erreur lors de la mise à jour de la tâche', error);
          });
      } else {
        console.error('Erreur : ID de tâche ou statut non trouvé');
      }

    }
  }

  refreshTaskLists() {
    this.toDoList = this.tasks.filter(task => task.status === 'To Do');
    this.inProgressList = this.tasks.filter(task => task.status === 'In Progress');
    this.inReviewList = this.tasks.filter(task => task.status === 'In Review');
    this.doneList = this.tasks.filter(task => task.status === 'Done');
  }


}
