import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {initializeApp} from "firebase/app";
import { getFirestore, collection, getDocs, doc, updateDoc, addDoc, getDoc, deleteDoc, setDoc } from 'firebase/firestore';
import {getAnalytics, isSupported} from "firebase/analytics";
import { getAuth, signInWithEmailAndPassword, createUserWithEmailAndPassword, signOut } from 'firebase/auth';
import firebase from "firebase/compat";
import Firestore = firebase.firestore.Firestore;


const firebaseConfig = {
  apiKey: "AIzaSyC4Y_6d2UbbeEILXPIXLceinVGSpbJteKc",
  authDomain: "projectwebtasks.firebaseapp.com",
  databaseURL: "https://projectwebtasks-default-rtdb.firebaseio.com",
  projectId: "projectwebtasks",
  storageBucket: "projectwebtasks.appspot.com",
  messagingSenderId: "944617784538",
  appId: "1:944617784538:web:8c958460f5be13688c91e3",
  measurementId: "G-V6ZBDJY4XL"
};

const app = initializeApp(firebaseConfig);


isSupported().then(supported => {
  if (supported) {
    const analytics = getAnalytics(app);
  } else {
    console.log('Firebase Analytics not supported in this environment');
  }
});

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  private db = getFirestore(app);
  private auth = getAuth(app);
  private currentUserSubject = new BehaviorSubject<any>(null);

  constructor() {
    this.auth.onAuthStateChanged(user => {
      this.currentUserSubject.next(user);
    });
  }

  isAuthenticated(): boolean {
    return !!this.currentUserSubject.value;
  }

  getTasks(): Observable<any[]> {
    return new Observable(observer => {
      const tasksCollection = collection(this.db, 'tasks');
      getDocs(tasksCollection)
        .then(querySnapshot => {
          const data = querySnapshot.docs.map(doc => ({
            id: doc.id,
            ...doc.data()
          }));
          observer.next(data);
          observer.complete();
        })
        .catch(error => observer.error(error));
    });
  }

  getUsers(): Observable<any[]> {
    return new Observable(observer => {
      const usersCollection = collection(this.db, 'users');
      getDocs(usersCollection)
        .then(querySnapshot => {
          const data = querySnapshot.docs.map(doc => ({
            id: doc.id,
            ...doc.data()
          }));
          observer.next(data);
          observer.complete();
        })
        .catch(error => observer.error(error));
    });
  }

  updateStatusTask(taskId: string, taskData: any): Promise<void> {
    const taskDocRef = doc(this.db, 'tasks', taskId);
    return updateDoc(taskDocRef, taskData);
  }

  async saveTask(task: any): Promise<void> {
    if (task.id) {
      const taskRef = doc(this.db, 'tasks', task.id);
      await setDoc(taskRef, task);
    } else {
      const tasksCol = collection(this.db, 'tasks');
      const docRef = await addDoc(tasksCol, task);
      console.log('Nouvelle tâche créée avec ID:', docRef.id);
      await setDoc(docRef, { ...task, id: docRef.id });
    }
  }

  deleteTask(taskId: string): Promise<void> {
    const taskDocRef = doc(this.db, 'tasks', taskId);
    return deleteDoc(taskDocRef);
  }

  signInWithEmail(email: string, password: string): Promise<any> {
    return signInWithEmailAndPassword(this.auth, email, password)
      .then((userCredential) => {
        const user = userCredential.user;
        if (user) {
          const userRef = doc(this.db, 'users', user.uid);
          return getDoc(userRef).then((docSnapshot: { exists: () => any; data: () => any; }) => {
            if (docSnapshot.exists()) {
              return docSnapshot.data();
            } else {
              throw new Error('User does not exist in Firestore');
            }
          });
        } else {
          throw new Error('User not found');
        }
      })
      .catch((error) => {
        console.error("Error signing in: ", error);
        throw error;
      });
  }


  signUpWithEmail(email: string, password: string): Promise<any> {
    return createUserWithEmailAndPassword(this.auth, email, password)
      .then((userCredential) => {
        const user = userCredential.user;
        if (user) {
          const userRef = collection(this.db, 'users');
          return addDoc(userRef, {
            uid: user.uid,
            email: user.email,
          });
        } else {
          throw new Error('User not found after sign up');
        }
      })
      .catch((error) => {
        console.error("Error signing up: ", error);
        throw error;
      });
  }

  signOut(): Promise<void> {
    return signOut(this.auth);
  }
}


