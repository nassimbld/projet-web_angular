import {Component, EventEmitter, Output} from '@angular/core';
import { CommonModule } from '@angular/common';
import {DialogComponent} from "../dialog/dialog.component";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './header.component.html',
  styleUrl: './header.component.css'
})
export class HeaderComponent {
  @Output() update: EventEmitter<void> = new EventEmitter();

  constructor(public dialog: MatDialog) {

  }

  openCreateTaskDialog(): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '250px',
      data: {status: 'To Do'}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Tâche à créer:', result);
      }
      this.update.emit();
    });
  }

}
