import { Component } from '@angular/core';
import {FirebaseService} from "../services/firebase-service.service";
import {FormsModule} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    FormsModule
  ],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {
  email = '';
  password = '';
  isLoading = false;

  constructor(private firebaseService: FirebaseService,
              private router: Router) {}

  navigateToRegister() {
    this.router.navigate(['/register']);
  }

  async loginUser() {
    this.firebaseService.signInWithEmail(this.email, this.password)
      .then(userData => {
        this.router.navigate(['']);
      })
      .catch(error => {

      });
  }

}
