import { Component } from '@angular/core';
import {FirebaseService} from "../services/firebase-service.service";
import {FormsModule} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  standalone: true,
  imports: [
    FormsModule
  ],
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent {
  email = '';
  password = '';
  isLoading = false;

  constructor(private firebaseService: FirebaseService,
              private router: Router) {}

  navigateToLogin() {
    this.router.navigate(['/login']);
  }

  async registerUser() {
    this.firebaseService.signUpWithEmail(this.email, this.password)
      .then(userData => {
        this.router.navigate(['/login']);
      })
      .catch(error => {
      });
  }

}
