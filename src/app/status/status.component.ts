import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  CdkDrag,
  CdkDragDrop,
  CdkDragEnd,
  CdkDropList,
  moveItemInArray,
  transferArrayItem
} from "@angular/cdk/drag-drop";
import {MatCardModule} from "@angular/material/card";
import {FirebaseService} from "../services/firebase-service.service";
import {DialogComponent} from "../dialog/dialog.component";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";

interface Task {
  id: string;
  assignedUserId: string;
  description: string;
  status: string;
  title: string;
}

@Component({
  selector: 'app-status',
  standalone: true,
  imports: [CommonModule, CdkDropList, CdkDrag, MatCardModule],
  templateUrl: './status.component.html',
  styleUrl: './status.component.css'
})
export class StatusComponent {
  @Input() status: any;
  @Input() tasks: Task[] = [];
  @Output() update: EventEmitter<void> = new EventEmitter();

  constructor(private firebaseService: FirebaseService,
              public dialog: MatDialog) {}

  editTask(taskId: string) {
    console.log('Éditer la tâche', taskId);
  }

  deleteTask(taskId: string) {
    if(confirm('Êtes-vous sûr de vouloir supprimer cette tâche ?')) {
      this.firebaseService.deleteTask(taskId).then(() => {
        console.log('Tâche supprimée avec succès');
        this.update.emit();
      }).catch(error => {
        console.error('Une erreur est survenue lors de la suppression de la tâche', error);
      });
    }
    console.log('Supprimer la tâche', taskId);
  }

  openCreateTaskDialog(task: Task): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '250px',
      data: {id: task.id, assignedUserId: task.assignedUserId, title: task.title, description: task.description, status: this.status}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Tâche à créer:', result);
      }
      this.update.emit();
    });
  }
}
